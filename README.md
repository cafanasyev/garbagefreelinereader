# GarbageFreeLineReader

### DESCRIPTION:
* Simple library that reads files line by line and produces **zero** garbage. 
* The "Garbage Free" behaviour is achieved by reading files into a mutable byte array.
* This library contains two main interfaces: `ILineReader` and `ILineBuffer`.
* The `ILineReader` interface contains the `nextLine()` method with the return type 
  of `ILineBuffer` interface.
* The `ILineBuffer` interface implementation contains byte array and two `int` variables (`from` and `to`)
  indicating interval in which a current next line resides.

### USAGE EXAMPLE:

###### INPUT TXT FILE:
```
1234567890
aaaaaaaaaaaaaa
bbbbbbbbbbbbbbbbbbbbbbbb
```
###### CODE:
``` java
    public void example(InputStream inputStream) {
        int bufferLength = 75; // set hard limit on buffer size (default size is 10000)
        ILineReader lineReader = new BufferedLineReader(inputStream, bufferLength);
        
        for(int i = 1; i <= 3; i++) {
            ILineBuffer nextLine = lineReader.nextLine();
            byte[] line = nextLine.getLine();
            int from = nextLine.getFrom();
            int to = nextLine.getTo();

            System.out.println("iteration number: " + i);
            System.out.println("input byte array as String: " );
            System.out.println(new String(line, StandardCharsets.UTF_8));
            System.out.println("from: " + from);
            System.out.println("to: " + to);
            System.out.println("current line text: " + convertBytesIntoString(line, from, to - from + 1));
            System.out.println();
        }
    }

    public String convertBytesIntoString(byte[] line, int offset, int length) {
        return new String(line, offset, length, Charset.defaultCharset());
    }
```
###### OUTPUT:
```
Iteration number: 1
input byte array as String: 
1234567890
aaaaaaaaaaaaaa
bbbbbbbbbbbbbbbbbbbbbbbb
NULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNUL
from: 0
to: 9
current line text: 1234567890

Iteration number: 2
input byte array as String: 
1234567890
aaaaaaaaaaaaaa
bbbbbbbbbbbbbbbbbbbbbbbb
NULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNUL
from: 11
to: 24
current line text: aaaaaaaaaaaaaa

Iteration number: 3
input byte array as String: 
1234567890
aaaaaaaaaaaaaa
bbbbbbbbbbbbbbbbbbbbbbbb
NULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNULNUL
from: 26
to: 49
current line text: bbbbbbbbbbbbbbbbbbbbbbbb
```
