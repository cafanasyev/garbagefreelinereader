object Plugins {
    object Versions {
        const val kotlin_jvm_plugin = "1.5.10"
    }
}

object Dependencies {
    object Versions {
        const val lombok = "1.18.20"
    }
    // https://mvnrepository.com/artifact/org.projectlombok/lombok
    const val lombok = "org.projectlombok:lombok:${Versions.lombok}"
    // Align versions of all Kotlin components
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-bom
    const val kotlin_bom = "org.jetbrains.kotlin:kotlin-bom"
}

object TestDependencies {
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-test - Kotlin test library.
    const val kotlin_test = "org.jetbrains.kotlin:kotlin-test"
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-params
    const val junit_jupiter_params = "org.junit.jupiter:junit-jupiter-params"
}
