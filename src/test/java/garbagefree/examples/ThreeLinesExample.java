package garbagefree.examples;

import garbagefree.linereader.io.BufferedLineReader;
import garbagefree.linereader.io.ILineBuffer;
import garbagefree.linereader.io.ILineReader;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ThreeLinesExample {
    public static void main(String[] args) {
        InputStream is = ThreeLinesExample.class.getResourceAsStream("/threeLines.txt");
        new ThreeLinesExample().example(is);
    }

    public void example(InputStream inputStream) {
        int bufferLength = 75; // set hard limit on buffer size (default size is 10000)
        ILineReader lineReader = new BufferedLineReader(inputStream, bufferLength);
        
        for(int i = 1; i <= 3; i++) {
            ILineBuffer nextLine = lineReader.nextLine();
            byte[] line = nextLine.getLine();
            int from = nextLine.getFrom();
            int to = nextLine.getTo();

            System.out.println("Iteration number: " + i);
            System.out.println("input byte array as String: " );
            System.out.println(new String(line, StandardCharsets.UTF_8));
            System.out.println("from: " + from);
            System.out.println("to: " + to);
            System.out.println("current line text: " + convertBytesIntoString(line, from, to - from + 1));
            System.out.println();
        }
    }

    public String convertBytesIntoString(byte[] line, int offset, int length) {
        return new String(line, offset, length, Charset.defaultCharset());
    }
}
