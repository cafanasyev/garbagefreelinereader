package garbagefree.linereader.testUtils

import java.io.InputStream


fun String.resourceAsStream(): InputStream? = object {}.javaClass.getResourceAsStream(this)
