package garbagefree.linereader.io

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import garbagefree.linereader.io.Constants.ASCII_NEXT_LINE
import garbagefree.linereader.testUtils.resourceAsStream
import java.io.InputStream
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull


class BufferedLineReaderTest {
    private var inputStream: InputStream? = null
    private lateinit var bufferedLineReader: ILineReader
    
    private val expectedLines = 
        arrayOf("","1234567890", "aaaaaaaaaaaaaa",
                "bbbbbbbbbbbbbbbbbbbbbbbb", "cccccccccccccccccccccccc",
                "ddddddddddddddddddddddd", "eeeeeeeeeeeeeeeeeeeeeee")
    
    @ParameterizedTest
    @CsvSource("1,0,9","2,11,24","3,26,49","4,51,74","5,76,98")
    fun `verify calling nextLine() returns line ending exact before the next line char`(
        times: Int, from: Int, to: Int) 
    {
        // given
        inputStream = "/sixLines.csv".resourceAsStream()
        bufferedLineReader = BufferedLineReader(inputStream!!)
        var lineBuffer: ILineBuffer? = null
        // when
        repeat(times) { lineBuffer = bufferedLineReader.nextLine() }
        // then
        assertEquals(from, lineBuffer?.from)
        assertEquals(to, lineBuffer?.to)
        assertEquals(ASCII_NEXT_LINE, lineBuffer?.line?.get(to+1))
        assertEquals(expectedLines[times], lineBuffer.toString())
    }

    @ParameterizedTest
    @CsvSource("1,0,9","2,11,24","3,0,23","4,25,48","5,0,22,","6,24,46")
    fun `verify buffer swap`(times: Int, from: Int, to: Int) {
        // given
        inputStream = "/sixLines.csv".resourceAsStream()
        bufferedLineReader = BufferedLineReader(inputStream!!, 50 /*specify small buffer*/)
        var lineBuffer: ILineBuffer? = null
        // when
        repeat(times) { lineBuffer = bufferedLineReader.nextLine() }
        // then
        assertEquals(from, lineBuffer?.from)
        assertEquals(to, lineBuffer?.to)
        assertEquals(expectedLines[times], lineBuffer.toString())
    }
    
    @Test
    fun `verify netLine() returns null after every invocation when no more lines to read`() {
        // given
        inputStream = "/threeLines.txt".resourceAsStream() // buffer ends right on ASCII_NEXT_LINE
        bufferedLineReader = BufferedLineReader(inputStream!!, 50 /*specify small buffer*/)
        var lineBuffer: ILineBuffer? = null
        // when
        repeat(4) { lineBuffer = bufferedLineReader.nextLine() }
        // then
        assertNull(lineBuffer)
        // and again
        repeat(3) { assertNull(bufferedLineReader.nextLine()) }
    }
    
    // TODO: case when several NEXT_LINE characters one after another
    
    // TODO: case when file starts with NEXT_LINE characters

    // TODO: case when file starts with several NEXT_LINE characters
    
    // TODO: case when file ends with NEXT_LINE character
    
    // TODO: case when file ends with several NEXT_LINE characters
    
}
