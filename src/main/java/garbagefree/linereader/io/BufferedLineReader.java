package garbagefree.linereader.io;

import garbagefree.linereader.annotations.NotThreadSafe;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.Setter;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;


@NotThreadSafe
public class BufferedLineReader implements ILineReader {
    private final int bufferLength;
    private final InputStream inputStream;
    private LineBuffer line = new LineBuffer(); // line is just a container to return the line read
    
    private byte[] byteBuffer;
    private byte[] emptyByteBuffer;

    private long qtyOfLinesHaveRead = 0;
    private int bytesRead = 0; // bytes read from InputStream into the byteBuffer
    private int i = -1; // current index in the byteBuffer
    
    @Setter(AccessLevel.PRIVATE)
    private Supplier<ILineBuffer> lineProducer = () -> {
        initReader(); // Lazy init. Execute init only on first invocation.
        setLineProducer(this::tryReadNextLine);
        return tryReadNextLine();
    };

    public BufferedLineReader(@NonNull InputStream inputStream) {
        this.inputStream = inputStream;
        this.bufferLength = 10000;
        this.byteBuffer = new byte[this.bufferLength];
        this.emptyByteBuffer = new byte[this.bufferLength];
    }

    public BufferedLineReader(@NonNull InputStream inputStream, int bufferLength) {
        this.inputStream = inputStream;
        this.bufferLength = bufferLength;
        this.byteBuffer = new byte[this.bufferLength];
        this.emptyByteBuffer = new byte[this.bufferLength];
    }

    @Override
    public void close() throws Exception {
        inputStream.close();
    }
    
    @Override
    public ILineBuffer nextLine() {
        return lineProducer.get();
    }
    
    @Override
    public long getQtyOfLinesHaveRead() {
        return qtyOfLinesHaveRead;
    }

    private void initReader() {
        bytesRead = readNextBytes(byteBuffer, 0, bufferLength);
        line.setLine(byteBuffer);
        line.setTo(-2);
    }
    
    private LineBuffer tryReadNextLine() {
        do { i++; } while (i < (bytesRead - 1) && byteBuffer[i] != Constants.ASCII_NEXT_LINE);
        
        // "i < bufferLength" is a check for scenario when last character in the buffer is NEXT_LINE
        // so on next invocation of the nextLine() we would avoid ArrayIndexOutOfBoundsException 
        // and will try to swap buffers and read next bytes
        if (i < bufferLength && byteBuffer[i] == Constants.ASCII_NEXT_LINE) {
            line.setFrom(line.getTo() + 2);
            line.setTo(i - 1);
            qtyOfLinesHaveRead++;
        } else if (bytesRead < 1) {
            line = null;
            setLineProducer(() -> null); // always return null when reached the end of file
        } else if (i >= (bytesRead - 1)) { // try read more from the InputStream
            int len = bytesRead - (line.getTo() + 2);
            System.arraycopy(byteBuffer, line.getTo() + 2, emptyByteBuffer, 0, len);
            bytesRead = readNextBytes(emptyByteBuffer, len, bufferLength - len) + len;
            i = -1;
            swapBuffers();
            line.setLine(byteBuffer);
            line.setTo(-2);
            line = tryReadNextLine();
        } 
        
        return line;
    }

    private void swapBuffers() {
        byte[] temp = byteBuffer;
        byteBuffer = emptyByteBuffer;
        emptyByteBuffer = temp;
    }

    private int readNextBytes(byte[] b, int off, int len) {
        int bytesRed = 0;
        
        try {
            bytesRed = inputStream.read(b, off, len);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return bytesRed;
    }
}
