package garbagefree.linereader.io;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.nio.charset.Charset;

@Getter @Setter(AccessLevel.PACKAGE)
public class LineBuffer implements ILineBuffer {
    byte[] line;
    private int from = 0;
    private int to = 0;
    
    public String toString() {
        return new String(this.line, this.from, this.to - this.from + 1, Charset.defaultCharset());
    }
}
