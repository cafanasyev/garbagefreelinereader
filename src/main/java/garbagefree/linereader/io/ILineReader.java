package garbagefree.linereader.io;

public interface ILineReader extends AutoCloseable {
    ILineBuffer nextLine();
    long getQtyOfLinesHaveRead();
}
