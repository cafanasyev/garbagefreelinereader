package garbagefree.linereader.io;

public interface ILineBuffer {
    byte[] getLine();
    int getFrom();
    int getTo();
}
