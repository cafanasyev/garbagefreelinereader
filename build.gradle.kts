plugins {
    kotlin("jvm") version Plugins.Versions.kotlin_jvm_plugin
    `java-library`
    `maven-publish`
}

group = "io.gitlab.cafanasyev.garbagefree"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    annotationProcessor(Dependencies.lombok)
    compileOnly(Dependencies.lombok)
    
    implementation(platform(Dependencies.kotlin_bom))
    
    testImplementation(TestDependencies.kotlin_test)
    testImplementation(TestDependencies.junit_jupiter_params)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
    withJavadocJar()
}

tasks.test {
    useJUnitPlatform()
}
